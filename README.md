# Usage
1. Add this repo

        brew tap morenvino/symantec-homebrew https://bitbucket.org/morenvino/symantec-homebrew.git

2. Then install

        brew install git-credential-helper
        brew install cloud-gate
