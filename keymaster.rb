class Keymaster < Formula
  desc "Symantec Keymaster"
  url "https://mirror.symcpe.net/misc/keymaster/current/darwin-amd64/keymaster"
  
  version "1.7.0"
  sha256 "b56367acc7b03cf97f33e839c4d2b60de371096c1433ffa4be099792df0f773e"

  def install
    bin.install "keymaster"
  end

  test do
    system "#{bin}/keymaster", "-version"
  end
end
