class CloudGate < Formula
  desc "Symantec Cloud Gate"
  url "https://mirror.symcpe.net/misc/cloud-gate/current/cloud-gate.Darwin"
  
  version "0.4.2"
  sha256 "1b1c01b81faf6e9cec74e16234fbb90a4bbb627b1fb4f9b091a348812388600d"

  depends_on "keymaster"

  def install
    bin.install "cloud-gate.Darwin"
    mv "#{bin}/cloud-gate.Darwin", "#{bin}/cloud-gate"
  end

  test do
    system "#{bin}/cloud-gate", "-version"
  end
end
