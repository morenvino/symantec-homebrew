class GitCredentialHelper < Formula
  desc "Git Credential Helper for Symantec Stash"
  url "https://artifactory.ges.symantec.com/artifactory/ses-generic/git-credential-symantec-stash/darwin/git-credential-symantec-stash"
  
  version "1.1.0"
  sha256 "dfbf8268f7e4ec88e232e6790e0fc6b58827435f6939029bf43050011598df1f"
  
  def install
    bin.install "git-credential-symantec-stash"
  end

  # brew
  def caveats; <<~EOS
    Update your git config to finish installation (the Homebrew isolation seems to restrict this access):
    $ git config --global credential.https://stash.ges.symantec.com.helper symantec-stash
  EOS
  end

  test do
    system "#{bin}/git-credential-symantec-stash", "-version"
  end
end
